const modalWindowDeclarations = [
  {
    id: "modalID1",
    title: "Add this item to the cart?",
    description: "Once you add this item to the cart, you can view it in the list of your selected products.",
    backgroundColor: "rgba(17, 56, 128, 0.747)",
    headerBackgroundColor: "rgba(57, 17, 121, 0.747)",
    crossBtn: false
  },
  {
    id: "modalID2",
    title: "Do you want to delete this file?",
    description: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
    backgroundColor: "rgb(226, 7, 7)",
    headerBackgroundColor: "rgba(175, 27, 27, 0.678)",
    crossBtn: true
  }
]

export { modalWindowDeclarations }