import React, { Component } from 'react';
import './App.scss';
import Button from './Components/Button/Button';
import Modal from './Components/Modal/Modal';
import { modalWindowDeclarations } from './ModalConfig'

class App extends Component {
  state = {
    noModals: true,
    modalContent: {}
  };

  openModalHandler = (e) => {
    const modalID = e.target.id;
    const modalDeclaration = modalWindowDeclarations.find(item => item.id === modalID);
    this.setState((prev) => {
      return {
        noModals: !prev.noModals,
        modalContent: modalDeclaration
      }
    });
  };
  closeModalHandler = () => {
    this.setState((prev) => {
      return {
        noModals: !prev.noModals,
        modalContent: {}
      }
    });
  };

  render() {
    const { title, description, backgroundColor, headerBackgroundColor, crossBtn } = this.state.modalContent;

    const actions = (
      <div className="modal__btn-container">
        <Button className="modal-btn" text="OK" />
        <Button className="modal-btn" text="Close" onClick={this.closeModalHandler} />
      </div>
    );

    if (this.state.noModals) {
      return (
        <div className="container">
          <div className="btnContainer">
            <Button
              btnID="modalID1"
              text="Open first modal"
              backgroundColor="rgba(255, 238, 0, 0.925)"
              onClick={this.openModalHandler}
              className='btn'
            />
            <Button
              btnID="modalID2"
              text="Open second modal"
              backgroundColor="aquamarine"
              onClick={this.openModalHandler}
              className='btn'
            />
          </div>
        </div>
      );
    }
    return (
      <div className="container">
        <Modal
          header={title}
          text={description}
          backgroundColor={backgroundColor}
          headerBackgroundColor={headerBackgroundColor}
          crossBtn={crossBtn}
          closeModal={this.closeModalHandler}
          actions={actions}
        />
      </div>
    )
  }
}

export default App;
