import React, { Component } from 'react';
import './Modal.scss';

class Modal extends Component {
  render() {
    const { header, crossBtn, actions, closeModal, text, backgroundColor, headerBackgroundColor } = this.props;

    return (
      <div className="modal-container"
        onClick={closeModal}>
        <div className="modal" style={{ backgroundColor: backgroundColor }} onClick={(e) => e.stopPropagation()}>
          <div className="modal__header" style={{ backgroundColor: headerBackgroundColor }}>
            <h2 className="modal__header__text">{header}</h2>
            {
              crossBtn && <span className="modal__header__cross" onClick={closeModal}>X</span>
            }
          </div>
          <p className="modal__text">{text}</p>
          {actions}
        </div>
      </div>
    );
  }
}

export default Modal;