import React, { Component } from 'react';
import './Button.scss';

class Button extends Component {

  render() {
    const { text, backgroundColor, onClick, btnID, className } = this.props;

    return (
      <button
        id={btnID}
        className={className}
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
      >{text}</button>
    )
  }
}

export default Button;